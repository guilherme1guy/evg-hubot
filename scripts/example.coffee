module.exports = (robot) ->

  rasa_host = "localhost:3000"

  if process.env['RASA_URL']
    rasa_host = process.env['RASA_URL']

  robot.catchAll (msg) ->

    data = JSON.stringify({
      message: msg.message.text
    })

    robot.http(rasa_host + '/webhooks/rest/webhook')
    .header('Content-Type', 'application/json')
    .post(data) (err, res, body) ->
      
      data = JSON.parse body
      data = data[0]

      msg.reply data.text
